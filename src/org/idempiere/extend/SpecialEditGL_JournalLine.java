/******************************************************************************
 * Copyright (C) 2012 Carlos Ruiz                                             *
 * Copyright (C) 2012 Trek Global                 							  *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.idempiere.extend;

import java.sql.Timestamp;

import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MJournal;
import org.compiere.model.MJournalLine;
import org.compiere.model.PO;
import org.compiere.model.X_C_Invoice;
import org.compiere.model.X_C_InvoiceLine;
import org.compiere.util.Env;
import org.idempiere.base.ISpecialEditCallout;
import org.idempiere.base.SpecialEditorUtils;

/**
 * @author Nicolas Micoud
 *
 */
public class SpecialEditGL_JournalLine implements ISpecialEditCallout {

	@Override
	public boolean canEdit(GridTab mTab, GridField mField, PO po) {
		// TODO Auto-generated method stub
		System.out.println("canEdit " + mTab + " - " + mField + " - "+ po);
		return true;
	}

	@Override
	public String validateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {
		// TODO Auto-generated method stub
		System.out.println("validateEdit " + mTab + " - " + mField + " - "+ po);
		return null;
	}

	@Override
	public boolean preEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("preEdit " + mTab + " - " + mField + " - "+ po);
		SpecialEditorUtils.deletePosting(new MJournal(Env.getCtx(), (Integer) mTab.getValue("GL_Journal_ID"), null));
		return true;
	}

	@Override
	public boolean updateEdit(GridTab mTab, GridField mField, PO po, Object newValue) {

		//		po.set_ValueOfColumn("C_Charge_ID", newValue);
		//		po.saveEx(); it can't be done using PO as you can't save a MInvoiceLine when its parent is processed

		//X_C_Invoice il = new X_C_Invoice(Env.getCtx(), mTab.getRecord_ID(), null);
		MJournalLine jl = new MJournalLine(Env.getCtx(), mTab.getRecord_ID(), null);
		int C_Project_ID=0;
		if(newValue!=null)
			C_Project_ID=(Integer) newValue;
		jl.setC_Project_ID(C_Project_ID);
		//il.setDateAcct((Timestamp) newValue);
		jl.saveEx();

		return true;
	}

	@Override
	public boolean postEdit(GridTab mTab, GridField mField, PO po) {
		System.out.println("postEdit " + mTab + " - " + mField + " - "+ po);
		// Repost invoice
		SpecialEditorUtils.post(mTab, new MJournal(Env.getCtx(), (Integer) mTab.getValue("GL_Journal_ID"), null));

		//Refresh
		SpecialEditorUtils.refresh(mTab);
		return true;
	}
}
